import { AddressDto } from './../dto/address.dto';
import { CartDto } from './cart.dto';
import { Document } from 'mongoose';
import { OrderStatus } from '../types/order-status.type';

export interface OrderDto {
  orderDate: number;
  customerId: string;
  status: OrderStatus;
  shippingAddress: AddressDto;
  subTotal: number;
  cart: CartDto[];
}

export interface OrderDocumentDto extends Document {
  orderDate: number;
  customerId: string;
  status: OrderStatus;
  shippingAddress: AddressDto;
  subTotal: number;
  cart: CartDto[];
}
