import { Document } from 'mongoose';

export interface AddressDto {
  city: string;
  state: string;
  street: string;
  number: string;
  zip: string;
  country: string;
  phone: string;
  fax: string;
}
export interface AddressDocumentDto extends Document {
  city: string;
  state: string;
  street: string;
  number: string;
  zip: string;
  country: string;
  phone: string;
  fax: string;
}
