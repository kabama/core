import { Document } from 'mongoose';
import { ProductDto } from '@shop/product-service/src/core/models/dto/product.dto';

export interface CartDto {
  id?: string;
  subTotal: number;
  products: ProductDto[];
}

export interface CartDocumentDto extends Document {
  id?: string;
  subTotal: number;
  products: ProductDto[];
}
