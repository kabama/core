export type OrderStatus = 'PURCHASED' | 'SHIPPED' | 'CANCELED';
