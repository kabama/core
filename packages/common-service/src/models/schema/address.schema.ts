import mongoose, { Schema } from 'mongoose';

import { AddressDocumentDto } from '../dto/address.dto';

export const AddressSchema = new Schema(
  {
    city: { type: String },
    state: { type: String },
    street: { type: String },
    number: { type: String },
    zip: { type: String },
    country: { type: String },
    phone: { type: String },
    fax: { type: String }
  },
  { timestamps: true }
);

export const Address = mongoose.model<AddressDocumentDto>('Address', AddressSchema);
