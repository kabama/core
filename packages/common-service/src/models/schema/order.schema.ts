import mongoose, { Schema } from 'mongoose';

import { AddressSchema } from './address.schema';
import { CartSchema } from './cart.schema';
import { OrderDocumentDto } from '../dto/order.dto';

const OrderSchema = new Schema({
  orderDate: { type: Number, required: true },
  customerId: mongoose.Types.ObjectId,
  status: { type: String, required: true },
  shippingAddress: AddressSchema,
  subTotal: { type: Number, required: true },
  cart: [CartSchema],
});

const Order = mongoose.model<OrderDocumentDto>('Order', OrderSchema);

export { Order };
