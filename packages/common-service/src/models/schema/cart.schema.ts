import mongoose, { Schema } from 'mongoose';

import { CartDocumentDto } from '../dto/cart.dto';
import { ProductSchema } from '@shop/product-service/src/core/models/schema/product.schema';

export const CartSchema = new Schema(
  {
    subTotal: { type: Number, required: true },
    products: [ProductSchema]
  },
  { timestamps: true }
);

const Cart = mongoose.model<CartDocumentDto>('Cart', CartSchema);

export { Cart };
