import dotenv from 'dotenv';

dotenv.config();
/** Configuraciones para mongoose */
const MONGO_OPTIONS = {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  socketTimeoutMS: 30000,
  keepAlive: true,
  poolSize: 50,
  autoIndex: false,
  retryWrites: false
};

const MONGO_USERNAME = process.env.MONGO_USERNAME || 'superuser';
const MONGO_PASSWORD = process.env.MONGO_USERNAME || 'supersecretpassword1';
const MONGO_HOST = process.env.MONGO_URL || 'ds343895.mlab.com:43895/mongobongo';
const MONGO = {
  host: MONGO_HOST,
  password: MONGO_PASSWORD,
  username: MONGO_USERNAME,
  options: MONGO_OPTIONS,
  //   url: `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOST}`,
  url: 'mongodb://localhost:27017/product'
};
/** Fin Configuraciones para mongoose */
const SERVER_HOSTNAME = process.env.SERVER_HOSTNAME || 'localhost';
const SERVER_PORT = process.env.SERVER_PORT || 3003;
/** Token  */
const SERVER_TOKEN_EXPIRETIME = process.env.SERVER_TOKEN_EXPIRETIME || 3600;
const SERVER_TOKEN_ISSUER = process.env.SERVER_TOKEN_ISSUER || 'coolIssuer';
const SERVER_TOKEN_SECRET = process.env.SERVER_TOKEN_SECRET || 'superencryptedsecret';

/** Rabbit */
const RABBIT_USERNAME = process.env.RABBIT_USERNAME || 'guest';
const RABBIT_PASSWORD = process.env.RABBIT_PASSWORD || 'Colombia2020#4';
const RABBIT_HOST = process.env.RABBIT_HOST || 'localhost';
const RABBIT_PROTOCOL = process.env.RABBIT_PASSWORD || 'amqp';

const RABBIT = {
  protocol: RABBIT_PROTOCOL,
  hostname: RABBIT_HOST,
  port: 5672,
  username: RABBIT_USERNAME,
  password: RABBIT_PASSWORD,
  vhost: '/',
  authMechanism: ['PLAIN', 'AMQPLAIN', 'EXTERNAL']
};
const SERVER = {
  hostname: SERVER_HOSTNAME,
  port: SERVER_PORT,
  token: {
    expireTime: SERVER_TOKEN_EXPIRETIME,
    issuer: SERVER_TOKEN_ISSUER,
    secret: SERVER_TOKEN_SECRET
  }
};

export const config = {
  mongo: MONGO,
  server: SERVER,
  rabbit: RABBIT,
  logLevel: 'Info'
};
