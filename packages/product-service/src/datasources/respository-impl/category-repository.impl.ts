import { Category } from './../../core/models/schema/category.schema';
import { CategoryDocumentDto } from './../../core/models/dto/category.dto';
import { CategoryRepository } from './../../core/repository/category/category.resposiory';

export class CategoryRepositoryImpl implements CategoryRepository {
  async create(item: CategoryDocumentDto): Promise<CategoryDocumentDto> {
    return await item.save();
  }
  update(id: string, item: CategoryDocumentDto): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  async delete(id: string): Promise<boolean> {
    return await Category.findByIdAndDelete(id)
      .then((result) => {
        if (result) {
          return true;
        }
      })
      .catch((err) => {
        return false;
      });
  }
  async find(): Promise<CategoryDocumentDto[]> {
    return await Category.find().exec();
  }
  async findOne(id: string): Promise<CategoryDocumentDto> {
    return await Category.findOne({ _id: id });
  }

  async findByName(name: string): Promise<CategoryDocumentDto> {
    return await Category.findOne({ name });
  }
}
