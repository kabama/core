import { Product } from './../../core/models/schema/product.schema';
import { ProductDocumentDto } from './../../core/models/dto/product.dto';
import { ProductRepository } from '../../core/repository/product/product-repository';

export class ProductRepositoryImpl implements ProductRepository {
  async create(item: ProductDocumentDto): Promise<ProductDocumentDto> {
    return await item.save();
  }
  async update(id: string, item: ProductDocumentDto): Promise<boolean> {
    return await item
      .updateOne({ ...item })
      .then((result) => {
        if (result) return true;
      })
      .catch(() => false);
  }
  async delete(id: string): Promise<boolean> {
    return await Product.deleteOne({ _id: { $eq: id } })
      .then((result) => {
        if (result) return true;
      })
      .catch(() => false);
  }
  async find(): Promise<ProductDocumentDto[]> {
    return await Product.find().exec();
  }
  async findOne(id: string): Promise<ProductDocumentDto> {
    return await Product.findOne({ id });
  }
}
