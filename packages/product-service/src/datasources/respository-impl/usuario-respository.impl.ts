import * as Models from '../../core/entities/interfaces';

import { User } from '../../core/entities/models/user/user.schema';
import { UserRepository } from '../../core/repositories/user/user.repository';

export class UserImplRepository implements UserRepository {
  async save(user: Models.IUser): Promise<Models.IUser> {
    return await user.save();
  }

  async getAll(): Promise<Models.IUser[]> {
    return await User.find().exec();
  }

  async findByUsername(username: string): Promise<Models.IUser> {
    return await User.findOne({ username });
  }

  async findByEmail(email: string): Promise<Models.IUser> {
    return await User.findOne({ email });
  }

  async findById(id: string): Promise<Models.IUser> {
    return await User.findById(id);
  }

  async findByResetTokenAndExpire(resetPasswordToken: string): Promise<Models.IUser> {
    const user = await User.findOne({
      resetPasswordToken,
      resetPasswordExpires: { $gt: Date.now() }
    });

    if (!user) {
      throw new Error('User token has expired');
    }
    return user;
  }
}
