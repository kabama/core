import { LogService } from './../config/logging';
import Mongoose from 'mongoose';
import { config } from '../config/environments/config';

Mongoose.Promise = global.Promise;

export class ConnectionDb {
  static NAMESPACE: string = 'ConnectionDb';
  static mongooseInstance: any;
  static mongooseConnection: Mongoose.Connection;

  public static mongooseDB() {
    /** Conexion a mongodb */
    return Mongoose.connect(config.mongo.url, { useUnifiedTopology: true, useNewUrlParser: true });
  }
  constructor() {
    ConnectionDb.connect();
  }

  static connect(): Mongoose.Connection {
    if (this.mongooseInstance) {
      return this.mongooseInstance;
    }

    this.mongooseConnection = Mongoose.connection;
    let logger = LogService.getInstance();
    this.mongooseConnection.once('open', () => {
      logger.info(this.NAMESPACE, 'Connect to an mongodb is opened.');
    });

    this.mongooseInstance = Mongoose.connect(config.mongo.url);

    this.mongooseConnection.on('connected', () => {
      logger.info(this.NAMESPACE, 'Mongoose default connection open to ' + config.mongo.url);
    });

    // If the connection throws an error
    this.mongooseConnection.on('error', (msg) => {
      logger.info(this.NAMESPACE, 'Mongoose default connection message:', msg);
    });

    // When the connection is disconnected
    this.mongooseConnection.on('disconnected', () => {
      setTimeout(function () {
        this.mongooseInstance = Mongoose.connect(config.mongo.url, {
          useUnifiedTopology: true,
          useNewUrlParser: true
        });
      }, 10000);
      logger.info(this.NAMESPACE, 'Mongoose default connection disconnected.');
    });

    // When the connection is reconnected
    this.mongooseConnection.on('reconnected', () => {
      logger.info(this.NAMESPACE, 'Mongoose default connection is reconnected.');
    });

    // If the Node process ends, close the Mongoose connection
    process.on('SIGINT', () => {
      this.mongooseConnection.close(() => {
        logger.info(
          this.NAMESPACE,
          'Mongoose default connection disconnected through app termination.'
        );
        process.exit(0);
      });
    });

    return this.mongooseInstance;
  }
}
