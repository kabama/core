import { ProductRepository } from '../../../repository/product/product-repository';

export class DeleteProductUseCase {
  constructor(private readonly productRepository: ProductRepository) {}

  async execute(id: string) {
    const isDeleted = await this.productRepository.delete(id);
    if (!isDeleted) {
      throw new Error(`The product with id ${id} not has be deleted`);
    }

    return isDeleted;
  }
}
