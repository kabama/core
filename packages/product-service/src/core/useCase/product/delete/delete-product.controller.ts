import { INTERNAL_SERVER_ERROR, NO_CONTENT } from 'http-status';
import { Request, Response } from 'express';

import { DeleteProductUseCase } from './delete-product.case';

export class DeleteProductController {
  constructor(private readonly deleteProductUseCase: DeleteProductUseCase) {}

  async handle(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const isDeleted = await this.deleteProductUseCase.execute(id);

      return res.status(NO_CONTENT).json({ data: isDeleted });
    } catch (error) {
      return res.status(INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
}
