import { DeleteProductController } from './delete-product.controller';
import { DeleteProductUseCase } from './delete-product.case';
import { ProductRepositoryImpl } from '../../../../datasources/respository-impl/product-repository.impl';

const pruductRepository = new ProductRepositoryImpl();
const deleteProductUseCase = new DeleteProductUseCase(pruductRepository);

const deleteProductController = new DeleteProductController(deleteProductUseCase);

export { deleteProductUseCase, deleteProductController };
