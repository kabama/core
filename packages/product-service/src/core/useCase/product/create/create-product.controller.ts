import { CREATED, INTERNAL_SERVER_ERROR } from 'http-status';
import { Request, Response } from 'express';

import { CreateProductUseCase } from './create-usecase';

export class CreateProductController {
  constructor(private readonly createProductUseCase: CreateProductUseCase) {}

  async handle(req: Request, res: Response) {
    const product = req.body;
    try {
      this.createProductUseCase.execute(product);
      return res.status(CREATED).json({ message: 'Product has be created', success: true });
    } catch (error) {
      return res
        .status(INTERNAL_SERVER_ERROR)
        .json({ message: 'Product has not be created', success: false });
    }
  }
}
