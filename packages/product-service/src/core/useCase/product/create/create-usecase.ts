import { CreateProductProducerProvider } from './../../../providers/product-producer.provider';
import { Product } from './../../../models/schema/product.schema';
import { ProductDto } from 'core/models/dto/product.dto';
import { ProductRepository } from './../../../repository/product/product-repository';

export class CreateProductUseCase {
  constructor(
    private readonly productRepository: ProductRepository,
    private createProducer: CreateProductProducerProvider
  ) {}

  async execute(product: ProductDto): Promise<void> {
    const newProduct = new Product({
      ...product
    });
    this.productRepository
      .create(newProduct)
      .then((product) => {
        this.createProducer.sendToQueueProducer('product', product);
      })
      .catch((err) => {
        throw new Error(err.message);
      });
  }
}
