import { CreateProductController } from './create-product.controller';
import { CreateProductProducerProviderImpl } from './../../../providers/impl/product-producer-provider.impl';
import { CreateProductUseCase } from './create-usecase';
import { ProductRepositoryImpl } from './../../../../datasources/respository-impl/product-repository.impl';

const productRepository = new ProductRepositoryImpl();
const productProducer = new CreateProductProducerProviderImpl();
const createProductUseCase = new CreateProductUseCase(productRepository, productProducer);
const createProductController = new CreateProductController(createProductUseCase);

export { createProductUseCase, createProductController };
