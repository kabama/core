import { NO_CONTENT, OK } from 'http-status';
import { Request, Response } from 'express';

import { AllProductUseCase } from './allproduct-usecase';

export class AllProductController {
  constructor(private readonly allProductUseCase: AllProductUseCase) {}
  async handle(req: Request, res: Response) {
    try {
      const users = await this.allProductUseCase.execute();

      return res.status(OK).json({
        data: users,
        count: users.length
      });
    } catch (error) {
      return res.status(NO_CONTENT).json({ message: error.message });
    }
  }
}
