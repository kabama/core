import { AllProductController } from './allproduct.controller';
import { AllProductUseCase } from './allproduct-usecase';
import { ProductRepositoryImpl } from './../../../../datasources/respository-impl/product-repository.impl';

const productRepositoryImpl = new ProductRepositoryImpl();
const allProductUsecase = new AllProductUseCase(productRepositoryImpl);

const allProductController = new AllProductController(allProductUsecase);

export { allProductUsecase, allProductController };
