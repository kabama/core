import { ProductDocumentDto, ProductDto } from './../../../models/dto/product.dto';
import { ProductRepository } from './../../../repository/product/product-repository';

export class AllProductUseCase {
  constructor(private readonly productRepository: ProductRepository) {}

  async execute(): Promise<ProductDto[]> {
    const products = await this.productRepository.find();

    if (!products || !products.length) {
      throw new Error('Not exists products');
    }

    return products.map((value) => {
      return this.mapper(value);
    });
  }

  mapper(result: ProductDocumentDto) {
    const {
      id,
      name,
      pricing,
      assets,
      shipping,
      quantity,
      sku,
      type,
      description,
      feedbackEmail,
      ownerId,
      brand,
      category
    } = result;
    return <ProductDto>{
      id,
      name,
      pricing,
      assets,
      shipping,
      quantity,
      sku,
      type,
      description,
      feedbackEmail,
      ownerId,
      brand,
      category
    };
  }
}
