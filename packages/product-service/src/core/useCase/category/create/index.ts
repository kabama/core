import { CategoryRepositoryImpl } from './../../../../datasources/respository-impl/category-repository.impl';
import { CreateCategoryController } from './create-category.controller';
import { CreateCategoryUseCase } from './create-category.usecase';

const categoryRepository = new CategoryRepositoryImpl();
const createCategoryUseCase = new CreateCategoryUseCase(categoryRepository);
const createCategoryController = new CreateCategoryController(createCategoryUseCase);

export { createCategoryUseCase, createCategoryController };
