import { Category } from './../../../../core/models/schema/category.schema';
import { CategoryDto } from './../../../models/dto/category.dto';
import { CategoryRepository } from './../../../repository/category/category.resposiory';

export class CreateCategoryUseCase {
  constructor(private readonly categoryRepository: CategoryRepository) {}

  async execute(category: CategoryDto) {
    const {
      name,
      catalogId,
      childCategories,
      parentCategories,
      image,
      isRoot,
      isActive,
      seo
    } = category;

    const categoryDocument = new Category({
      name,
      catalogId,
      childCategories,
      parentCategories,
      image,
      isRoot,
      isActive,
      seo
    });
    const newCategory = await this.categoryRepository.create(categoryDocument);
    console.log(newCategory, 'newCategory');
    if (!newCategory) {
      throw new Error('Category has not be created');
    }
  }
}
