import { CREATED, INTERNAL_SERVER_ERROR } from 'http-status';
import { Request, Response } from 'express';

import { CreateCategoryUseCase } from './create-category.usecase';

export class CreateCategoryController {
  constructor(private readonly createCategoryUseCase: CreateCategoryUseCase) {}

  async handle(req: Request, res: Response) {
    const category = req.body;
    try {
      await this.createCategoryUseCase.execute(category);
      return res.status(CREATED).send();
    } catch (error) {
      return res.status(INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
}
