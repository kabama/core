import { NO_CONTENT, OK } from 'http-status';
import { Request, Response } from 'express';

import { AllCategoryUseCase } from './all-category.usecase';
import { CategoryDto } from './../../../models/dto/category.dto';

export class AllCategoryController {
  constructor(private readonly allCategoryUseCase: AllCategoryUseCase) {}

  async handle(req: Request, res: Response) {
    try {
      const categoryList: CategoryDto[] = await this.allCategoryUseCase.execute();
      console.log(categoryList, 'categoryList');
      return res.status(OK).json({
        data: categoryList,
        counter: categoryList.length
      });
    } catch (error) {
      return res.status(NO_CONTENT).json({ message: error.message });
    }
  }
}
