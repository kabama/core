import { CategoryDocumentDto, CategoryDto } from './../../../models/dto/category.dto';

import { CategoryRepository } from './../../../repository/category/category.resposiory';

export class AllCategoryUseCase {
  constructor(private readonly categoryRepository: CategoryRepository) {}

  async execute() {
    const categories = await this.categoryRepository.find();
    if (!categories || !categories.length) {
      throw new Error('Not content for categories!');
    }

    const categoryList: CategoryDto[] = categories.map((cat: CategoryDocumentDto) =>
      this.categoryMapper(cat)
    );
    return categoryList;
  }

  private categoryMapper(category: any) {
    const {
      id,
      name,
      catalogId,
      childCategories,
      parentCategories,
      image,
      isRoot,
      isActive,
      seo
    } = category;

    return {
      id,
      name,
      catalogId,
      childCategories,
      parentCategories,
      image,
      isRoot,
      isActive,
      seo
    };
  }
}
