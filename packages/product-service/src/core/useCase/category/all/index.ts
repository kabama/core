import { AllCategoryController } from './all-category.controller';
import { AllCategoryUseCase } from './all-category.usecase';
import { CategoryRepositoryImpl } from './../../../../datasources/respository-impl/category-repository.impl';

const categoryRepository = new CategoryRepositoryImpl();
const allCategoryUseCase = new AllCategoryUseCase(categoryRepository);
const allCategoryController = new AllCategoryController(allCategoryUseCase);

export { allCategoryUseCase, allCategoryController };
