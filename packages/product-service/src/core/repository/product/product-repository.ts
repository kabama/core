import { BaseRepository } from 'core/repository';
import { ProductDocumentDto } from '../../models/dto/product.dto';

export interface ProductRepository extends BaseRepository<ProductDocumentDto> {}
