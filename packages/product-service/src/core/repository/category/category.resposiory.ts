import { BaseRepository } from '../base.repository';
import { CategoryDocumentDto } from './../../models/dto/category.dto';

export interface CategoryRepository extends BaseRepository<CategoryDocumentDto> {}
