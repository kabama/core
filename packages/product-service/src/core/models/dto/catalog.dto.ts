import { Document } from 'mongoose';

export interface CatalogDto {
  id?: string;
  name: string;
}

export interface CatalogDocumentDto extends Document {
  id?: string;
  name: string;
}
