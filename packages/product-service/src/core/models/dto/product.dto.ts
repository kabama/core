import { Document } from 'mongoose';

export interface ProductDto {
  id?: string;
  name: string;
  sku: string;
  type: string;
  description?: string;
  feedbackEmail: string;
  ownerId: string;
  //marca
  brand?: BrandDto;
  //precios
  pricing: PricingDto;
  //envio
  shipping?: ShippingDto;
  quantity: number;
  assets: AssetDto;
  category: string;
}

export interface BrandDto {
  id: string;
  name: string;
}
export interface PricingDto {
  list: number;
  retail: number;
  savings: number;
  pct_savings: number;
}
export interface ShippingDto {
  weight: number;
  width: number;
  height: number;
  depth: number;
}

export interface ImageDto {
  width: string;
  height: string;
  src: string;
}

export interface AssetDto {
  imgs: ImageDto[];
}

export interface ProductDocumentDto extends Document {
  id?: string;
  name: string;
  sku: string;
  type: string;
  description?: string;
  feedbackEmail: string;
  ownerId: string;
  //marca
  brand: BrandDto;
  //precios
  pricing: PricingDto;
  //envio
  shipping?: ShippingDto;
  quantity: number;
  assets: AssetDto;
  category: string;
}
