import { Document } from 'mongoose';

export interface CategoryDto {
  name: string;
  catalogId: string;
  childCategories?: string[];
  parentCategories?: string[];
  image?: string;
  isRoot: boolean;
  isActive: boolean;
  seo?: SeoDto;
}

interface SeoDto {
  metaTitle?: string;
  metKeywords?: string;
  metaDescription?: string;
  slug?: string;
}

export interface CategoryDocumentDto extends Document {
  name: string;
  catalogId: string;
  childCategories?: string[];
  parentCategories?: string[];
  image?: string;
  isRoot: boolean;
  isActive: boolean;
  seo: SeoDto;
}
