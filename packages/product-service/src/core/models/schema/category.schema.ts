import mongoose, { Schema } from 'mongoose';

import { CategoryDocumentDto } from './../dto/category.dto';

const SeoSchema = new Schema({
  metaTitle: { type: String, required: false },
  metKeywords: { type: String, required: false },
  metaDescription: { type: String, required: false },
  slug: { type: String, required: false }
});

const CategorySchema = new Schema(
  {
    name: { type: String, require: true },
    catalogId: { type: String, require: true },
    childCategories: [String],
    parentCategories: [String],
    image: { type: String, require: false },
    isRoot: { type: Boolean, require: true },
    isActive: { type: Boolean, require: true },
    seo: SeoSchema
  },
  { timestamps: true }
);

const Category = mongoose.model<CategoryDocumentDto>('Category', CategorySchema);

export { Category, CategorySchema };
