import mongoose, { Schema } from 'mongoose';

import { CatalogDocumentDto } from './../dto/catalog.dto';

const CatalogSchema = new Schema(
  {
    name: { type: String, require: true }
  },
  { timestamps: true }
);

const Catalog = mongoose.model<CatalogDocumentDto>('Catalog', CatalogSchema);

export { Catalog };
