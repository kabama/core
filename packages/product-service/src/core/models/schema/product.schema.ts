import mongoose, { Schema } from 'mongoose';

import { ProductDocumentDto } from '../dto/product.dto';

const BrandSchema: Schema = new Schema({
  id: { type: String },
  name: { type: String, required: true }
});
const PricingSchema = new Schema({
  list: { type: Number },
  retail: { type: Number },
  savings: { type: Number },
  pct_savings: { type: Number }
});

const ShippingSDetailSchema = new Schema({
  weight: { type: Number },
  width: { type: Number },
  height: { type: Number },
  depth: { type: Number }
});

const ImageSchema = new Schema({
  width: { type: String, required: true },
  height: { type: String, required: true },
  src: { type: String, required: true }
});

const AssetsSchema = new Schema({
  imgs: [ImageSchema]
});

export const ProductSchema: Schema = new Schema(
  {
    sku: { type: String, required: true },
    type: { type: String, required: true },
    name: { type: String, required: true },
    description: { type: String },
    feedbackEmail: { type: String, required: true },
    ownerId: mongoose.Types.ObjectId,
    brand: BrandSchema,
    pricing: PricingSchema,
    shipping: ShippingSDetailSchema,
    quantity: { type: Number, required: true },
    assets: { type: AssetsSchema, required: false },
    category: mongoose.Types.ObjectId
  },
  { timestamps: true }
);
const Product = mongoose.model<ProductDocumentDto>('Producto', ProductSchema);
export { Product };
