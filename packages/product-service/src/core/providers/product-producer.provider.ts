import { ProductDto } from 'core/models/dto/product.dto';

export interface CreateProductProducerProvider {
  sendToQueueProducer(queue: string, data: ProductDto);
}
