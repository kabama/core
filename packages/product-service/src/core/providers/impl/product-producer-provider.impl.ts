import { CreateProductProducerProvider } from './../product-producer.provider';
import { ProductDto } from 'core/models/dto/product.dto';
import { RabbitConnect } from './../../../datasources/rabbit-connect';

export class CreateProductProducerProviderImpl implements CreateProductProducerProvider {
  constructor() {}

  sendToQueueProducer(queue: string, data: ProductDto) {
    RabbitConnect.getInstance(queue).sendToQueue(queue, data);
  }
}
