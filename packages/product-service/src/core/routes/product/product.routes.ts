import express, { Router } from 'express';

import { allProductController } from '../../useCase/product/all';
import { createProductController } from '../../useCase/product/create';
import { deleteProductController } from '../../useCase/product/delete';

export class ProductRouter {
  private router: Router = express.Router();
  create(): Router {
    return this.router.post('/create', (req, res) => createProductController.handle(req, res));
  }

  getAll(): Router {
    return this.router.get('/all', (req, res) => allProductController.handle(req, res));
  }

  delete(): Router {
    return this.router.delete('/:id', (req, res) => deleteProductController.handle(req, res));
  }
}
