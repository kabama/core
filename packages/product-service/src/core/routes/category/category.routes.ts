import express, { Router } from 'express';

import { allCategoryController } from './../../useCase/category/all';
import { createCategoryController } from './../../useCase/category/create';

export class CategoryRouter {
  private router: Router = express.Router();
  create(): Router {
    return this.router.post('/create', (req, res) => createCategoryController.handle(req, res));
  }

  allCategories(): Router {
    return this.router.get('/all', (req, res) => allCategoryController.handle(req, res));
  }
}
