import { CategoryRouter } from './category/category.routes';
import { ProductRouter } from './product/product.routes';
import { Router } from 'express';

const routes = Router();
const productRouter = new ProductRouter();
const categoryRouter = new CategoryRouter();

/** Router de productos */

routes.use('/product', productRouter.create());
routes.use('/product', productRouter.getAll());
routes.use('/product', productRouter.delete());

/** Router de categorias */

routes.use('/category', categoryRouter.create());
routes.use('/category', categoryRouter.allCategories());
export { routes };
