import winston from 'winston';

const env = 'development';

export class LogService {
  private logger: any;
  private static instance: LogService;
  constructor() {
    this.addLogger();
  }

  addLogger() {
    // Development Logger

    const options: winston.LoggerOptions = {
      transports: [
        new winston.transports.Console({
          level: process.env.NODE_ENV === 'production' ? 'error' : 'debug'
        }),
        new winston.transports.File({ filename: 'debug.log', level: 'debug' })
      ]
    };
    this.logger = winston.createLogger(options);

    this.logger.add(new winston.transports.Console());
    // process.on('unhandledRejection', function (reason, p) {
    //   this.logger.warn(
    //     'system level exceptions at, Possibly Unhandled Rejection at: Promise ',
    //     p,
    //     ' reason: ',
    //     reason
    //   );
    // });
  }
  info(namespace: string, message: string, object?: any) {
    if (object) {
      this.logger.info(`[${this.getTimeStamp()}] [INFO] [${namespace}] ${message}`, object);
    } else {
      this.logger.info(`[${this.getTimeStamp()}] [INFO] [${namespace}] ${message}`);
    }
  }

  warn(namespace: string, message: string, object?: any) {
    if (object) {
      this.logger.warn(`[${this.getTimeStamp()}] [WARN] [${namespace}] ${message}`, object);
    } else {
      this.logger.warn(`[${this.getTimeStamp()}] [WARN] [${namespace}] ${message}`);
    }
  }

  error(namespace: string, message: string, object?: any) {
    if (object) {
      this.logger.error(`[${this.getTimeStamp()}] [ERROR] [${namespace}] ${message}`, object);
    } else {
      this.logger.error(`[${this.getTimeStamp()}] [ERROR] [${namespace}] ${message}`);
    }
  }

  debug(namespace: string, message: string, object?: any) {
    if (object) {
      this.logger.debug(`[${this.getTimeStamp()}] [DEBUG] [${namespace}] ${message}`, object);
    } else {
      this.logger.debug(`[${this.getTimeStamp()}] [DEBUG] [${namespace}] ${message}`);
    }
  }

  getTimeStamp(): string {
    return new Date().toISOString();
  }

  static getInstance() {
    if (!this.instance) {
      return new LogService();
    }
    return LogService.instance;
  }
}
