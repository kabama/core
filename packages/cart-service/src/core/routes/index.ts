import { CartRouter } from './cart.router';
import { Router } from 'express';

const cartRoutes = new CartRouter();

const routes = Router();

routes.use('/cart', cartRoutes.create());
routes.use('/cart', cartRoutes.updateProduct());
routes.use('/cart', cartRoutes.deleteCart());
export { routes };
