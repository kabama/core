import express, { Request, Response, Router } from 'express';

import { cartUpdateCartController } from './../useCase/update-products';
import { createCartController } from './../useCase/create';
import { deleteCartController } from './../useCase/delete';

export class CartRouter {
  private router: Router = express.Router();
  constructor() {}
  create(): Router {
    return this.router.post('/create', (req: Request, res: Response) =>
      createCartController.handle(req, res)
    );
  }

  updateProduct(): Router {
    return this.router.put('/update-products', (req: Request, res: Response) =>
      cartUpdateCartController.handle(req, res)
    );
  }
  deleteCart(): Router {
    return this.router.delete('/:id', (req: Request, res: Response) =>
      deleteCartController.handle(req, res)
    );
  }
}
