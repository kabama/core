import mongoose, { Schema } from 'mongoose';

export const CartSchema = new Schema(
  {
    sku: { type: String, required: true },
    name: { type: String, required: true },
    price: { type: Number, required: true },
    color: { type: String, required: true },
    size: { type: Number, required: true },
    quantity: { type: Number, required: true },
    uin: { type: String, required: true }
  },
  { timestamps: true }
);

const Cart = mongoose.model('Cart', CartSchema);

export { Cart };
