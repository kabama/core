import { Schema } from 'mongoose';

const OrderSchema = new Schema({
  status: {},
  subTotal: { type: Number, required: true },
  orderDate: { type: Number, required: true }
});
