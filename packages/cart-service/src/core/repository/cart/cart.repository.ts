import { BaseRepository } from '../base.repository';
import { CartDocumentDto } from '@shop/common/src/models/dto/cart.dto';

export interface CartRepository extends BaseRepository<CartDocumentDto> {
  updateProductByCart(id: string, cart: CartDocumentDto): Promise<CartDocumentDto>;
}
