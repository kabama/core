import { CartRepositoryImpl } from './../../../datasources/respository-impl/cart-repository.impl';
import { CartUpdateProductsController } from './cart-update-products.controller';
import { CartUpdateProductsUseCase } from './cart-update-products.usecase';

const cartRepository = new CartRepositoryImpl();
const cartUpdateProductsUseCase = new CartUpdateProductsUseCase(cartRepository);

const cartUpdateCartController = new CartUpdateProductsController(cartUpdateProductsUseCase);

export { cartUpdateProductsUseCase, cartUpdateCartController };
