import { INTERNAL_SERVER_ERROR, OK } from 'http-status';
import { Request, Response } from 'express';

import { CartUpdateProductsUseCase } from './cart-update-products.usecase';

export class CartUpdateProductsController {
  constructor(private readonly cartUpdateProductsUseCase: CartUpdateProductsUseCase) {}

  handle(req: Request, res: Response) {
    const cart = req.body;
    try {
      this.cartUpdateProductsUseCase.execute(cart);
      return res.status(OK).send({ message: 'Cart has be updated' });
    } catch (error) {
      return res.status(INTERNAL_SERVER_ERROR).json({ message: 'Cart not be updated' });
    }
  }
}
