import { Cart } from '@shop/common/src/models/schema/cart.schema';
import { CartDto } from '@shop/common/src/models/dto/cart.dto';
import { CartRepository } from './../../repository/cart/cart.repository';

export class CartUpdateProductsUseCase {
  constructor(private readonly cartRepository: CartRepository) {}

  async execute(cart: CartDto) {
    const { id, products, subTotal } = cart;
    const cartDocument = new Cart({ id, products, subTotal });

    const result = await this.cartRepository.updateProductByCart(id, cartDocument);

    if (!result) {
      throw new Error(`Cart item not be updated`);
    }
  }
}
