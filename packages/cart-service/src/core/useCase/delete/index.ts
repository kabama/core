import { CartRepositoryImpl } from './../../../datasources/respository-impl/cart-repository.impl';
import { DeleteCartController } from './delete-cart.controller';
import { DeleteCartUseCase } from './delete-cart.usecase';

const cartRepository = new CartRepositoryImpl();
const deleteCartUseCase = new DeleteCartUseCase(cartRepository);
const deleteCartController = new DeleteCartController(deleteCartUseCase);

export { deleteCartUseCase, deleteCartController };
