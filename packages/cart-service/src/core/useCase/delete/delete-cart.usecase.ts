import { CartRepository } from './../../repository/cart/cart.repository';

export class DeleteCartUseCase {
  constructor(private readonly cartRepository: CartRepository) {}

  async execute(id: string) {
    const isDeleted = await this.cartRepository.delete(id);
    if (!isDeleted) {
      throw new Error('Cart not be deleted');
    }
    return isDeleted;
  }
}
