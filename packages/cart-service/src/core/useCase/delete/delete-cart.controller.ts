import { INTERNAL_SERVER_ERROR, NO_CONTENT } from 'http-status';
import { Request, Response } from 'express';

import { DeleteCartUseCase } from './delete-cart.usecase';

export class DeleteCartController {
  constructor(private readonly deleteCartUseCase: DeleteCartUseCase) {}

  async handle(req: Request, res: Response) {
    const { id } = req.params;
    try {
      await this.deleteCartUseCase.execute(id);
      return res.status(NO_CONTENT).send();
    } catch (error) {
      return res.status(INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
}
