import { Cart } from '@shop/common/src/models/schema/cart.schema';
import { CartDto } from '@shop/common/src/models/dto/cart.dto';
import { CartRepository } from './../../repository/cart/cart.repository';

export class CreateCartUseCase {
  constructor(private readonly cartRepository: CartRepository) {}

  async execute(cart: CartDto) {
    const { products, subTotal } = cart;

    const cartDocument = new Cart({
      products,
      subTotal
    });
    const result: CartDto = await this.cartRepository.create(cartDocument);
    if (!result) {
      throw new Error('Cart No can not be created');
    }
  }
}
