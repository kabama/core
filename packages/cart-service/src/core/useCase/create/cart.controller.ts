import { INTERNAL_SERVER_ERROR, OK } from 'http-status';
import { Request, Response } from 'express';

import { CartDto } from './../../../../../common-service/src/models/dto/cart.dto';
import { CreateCartUseCase } from './cart.usecase';

export class CreateCartController {
  constructor(private readonly createCartUseCase: CreateCartUseCase) {}
  async handle(req: Request, res: Response) {
    const cart: CartDto = req.body;
    try {
      await this.createCartUseCase.execute(cart);
      return res.status(OK).send();
    } catch (error) {
      return res.status(INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
}
