import { CartRepositoryImpl } from './../../../datasources/respository-impl/cart-repository.impl';
import { CreateCartController } from './cart.controller';
import { CreateCartUseCase } from './cart.usecase';

const cartRepository = new CartRepositoryImpl();
const createCartUseCase = new CreateCartUseCase(cartRepository);

const createCartController = new CreateCartController(createCartUseCase);

export { createCartUseCase, createCartController };
