import { Cart } from '@shop/common/src/models/schema/cart.schema';
import { CartDocumentDto } from '@shop/common/src/models/dto/cart.dto';
import { CartRepository } from './../../core/repository/cart/cart.repository';

export class CartRepositoryImpl implements CartRepository {
  async create(item: CartDocumentDto): Promise<CartDocumentDto> {
    return await item.save();
  }
  update(id: string, item: CartDocumentDto): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  async delete(id: string): Promise<boolean> {
    return await Cart.findByIdAndDelete(id)
      .then((result) => {
        if (result) {
          return true;
        }
      })
      .catch((err) => {
        return false;
      });
  }

  find(): Promise<CartDocumentDto[]> {
    return Cart.find().exec();
  }
  async findOne(id: string): Promise<CartDocumentDto> {
    return await Cart.findOne({ _id: id });
  }

  async findByName(name: string): Promise<CartDocumentDto> {
    return await Cart.findOne({ name });
  }

  async updateProductByCart(id: string, cart: CartDocumentDto): Promise<CartDocumentDto> {
    let filter = { _id: id };
    return await Cart.findOneAndUpdate(filter, { products: cart.products });
  }
}
