import * as amqp from 'amqplib';

import { config } from './../config/environments/config';

export class RabbitConnect {
  private static instance: RabbitConnect;
  conn: any;
  queue: any;
  channel: any;
  constructor(queue: any) {
    this.createConnection(queue);
  }
  async createConnection(queue: any) {
    this.conn = await amqp.connect(config.rabbit);
    this.channel = {};
    await this._createChannel(this.conn, queue);
  }
  async _createChannel(conn: any, queue: string) {
    this.channel = await conn.createChannel();
    if (this.channel) {
      this.queue = this.channel.assertQueue(queue);
    }
  }

  sendToQueue(queue: string, data: any) {
    const messageBuff = Buffer.from(JSON.stringify(data));
    this.channel.assertQueue(queue, { durable: true }).then((result: any) => {
      this.channel.sendToQueue(result.queue, messageBuff, { persistent: true });
    });
  }

  subscribeToQueue(queue: any) {
    this.channel.consume(queue, (message: any) => {
      const content = JSON.parse(message.content.toString());

      console.log(`Received message from "${queue}" queue`);
      console.log(content);

      this.channel.ack(message);
    });
  }

  public static getInstance(queue?: any) {
    if (!RabbitConnect.instance) {
      RabbitConnect.instance = new RabbitConnect(queue);
    }
    return RabbitConnect.instance;
  }
}
