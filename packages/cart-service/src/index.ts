import * as http from 'http';

import { ConnectionDb } from './datasources/connection.db';
import { LogService } from './config/logging';
import { RabbitConnect } from './datasources/rabbit-connect';
import { app } from './server';
import { config } from './config/environments/config';

const httpServer = http.createServer(app);
const logger = LogService.getInstance();
const NAMESPACE = 'INDEX';
ConnectionDb.connect();
RabbitConnect.getInstance('product');
httpServer.listen(config.server.port, () => {
  logger.info(NAMESPACE, `Server is running in ${config.server.hostname}:${config.server.port}`);
});
