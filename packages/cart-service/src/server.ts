import {
  NOT_FOUND_STATUS_CODE,
  NOT_FOUND_STATUS_MESSAGE,
  SUCCESS_RESPONSE_STATUS_CODE
} from './config/constants/constants';
import express, { NextFunction, Request, Response } from 'express';

import { LogService } from './config/logging';
import { Logger } from './config/logger/logger';
import { routes as apiRoutes } from './core/routes';
import bodyParser from 'body-parser';

const app = express();
const log = new Logger();
const logger = LogService.getInstance();
const NAMESPACE = 'server';

/** Configuracion de rutas */
/** Log the request */
app.use((req, res, next) => {
  /** Log the req */
  logger.info(
    NAMESPACE,
    `METHOD: [${req.method}] - URL: [${req.url}] - IP: [${req.socket.remoteAddress}]`
  );

  res.on('finish', () => {
    /** Log the res */
    logger.info(
      NAMESPACE,
      `METHOD: [${req.method}] - URL: [${req.url}] - STATUS: [${res.statusCode}] - IP: [${req.socket.remoteAddress}]`
    );
  });

  next();
});

/** Fin configuracion de rutas */

/** Parseo de cuerpo de request */
app.use(bodyParser.json({ limit: '50mb', type: 'application/json' }));
app.use(log.getRequestLogger());
app.use(bodyParser.urlencoded({ extended: true }));

/** Rules of our API */
app.use((req: Request, res: Response, next: NextFunction) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
    return res.status(200).json({});
  }

  next();
});
/** Fin Rules of our API */

/** Routes Auth */
app.use('/api/', apiRoutes);
app.use('/healt', (req: Request, res: Response) =>
  res.json({ success: true, status: SUCCESS_RESPONSE_STATUS_CODE, message: 'Healt Ok!' })
);

/** Error handling */
app.use((req, res, next) => {
  const error = new Error(NOT_FOUND_STATUS_MESSAGE);
  res.statusCode = NOT_FOUND_STATUS_CODE;
  res.send(error.message);
});

app.use(log.getRequestErrorLogger());

export { app };
