import mongoose from 'mongoose';
import { config } from '../config/environments/config';

export class ConnectionDb {
  static NAMESPACE: string = 'ConnectionDb';

  public static mongooseDB() {
    /** Conexion a mongodb */
    return mongoose.connect(config.mongo.url, config.mongo.options);
  }
}
