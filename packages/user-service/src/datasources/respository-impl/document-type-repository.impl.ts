import { DocumentTypeRepository } from 'core/repositories';
import * as Doc from '../../core/entities/interfaces';

import DocumentType from '../../core/entities/models/user/tipo-documento.schema';

export class DocumentTypeRepositoryImpl implements DocumentTypeRepository {
  async findAll(): Promise<Doc.IDocumentTypeDoc[]> {
    return DocumentType.find().exec();
  }

  async save(documentType: Doc.IDocumentTypeDoc): Promise<void> {
    await documentType.save();
  }

  async findOne(id: string): Promise<Doc.IDocumentTypeDoc> {
    return DocumentType.findById(id);
  }

  async deleteById(id: string): Promise<void> {
    await DocumentType.findOneAndDelete({ _id: id });
  }
}
