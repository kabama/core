import * as http from 'http';

import { ConnectionDb } from './datasources/connection.db';
import { LogService } from './config/logging';
import { app } from './server';
import { config } from './config/environments/config';

const httpServer = http.createServer(app);
const NAMESPACE = 'Index';

/** Conexion a mongodb */
ConnectionDb.mongooseDB()
  .then((result) => {
    console.log(ConnectionDb.NAMESPACE, 'Conectado establecida con servidor mongoDb');

    httpServer.listen(config.server.port, () =>
      LogService.info(
        NAMESPACE,
        `Server is running ${config.server.hostname}:${config.server.port}`
      )
    );
  })
  .catch((err) => {
    console.log(ConnectionDb.NAMESPACE, err.message, err);
  });
