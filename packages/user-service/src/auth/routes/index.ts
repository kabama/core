import { Router } from 'express';
import { AuthRouter } from './auth.routes';

const routes = Router();

const authRouter = new AuthRouter();

routes.use('/auth/', authRouter.validate());
routes.use('/auth/', authRouter.registerUsuario());
routes.use('/auth/', authRouter.login());
routes.use('/auth/', authRouter.getAll());
routes.use('/auth/', authRouter.findById());
routes.use('/auth/', authRouter.recoverPassword());
routes.use('/auth/', authRouter.resetPassword());

export { routes };
