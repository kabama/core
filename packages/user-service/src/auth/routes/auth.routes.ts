import express, { Router } from 'express';
import * as Middleware from '../../middleware/auth/extractJWT';

import { createUserController } from '../../core/useCases/user/create';
import { findByidController } from '../../core/useCases/user/findbyid';
import { loginController } from '../../core/useCases/user/login';
import { recoverPasswordController } from '../../core/useCases/auth/recoverpassword';
import { resetPasswordController } from '../../core/useCases/auth/resetpassword';
import { userListController } from '../../core/useCases/user/list';

export class AuthRouter {
  private router = express.Router();

  validate(): Router {
    return this.router.get('/validate', (req, res) => createUserController.handle(req, res));
  }

  registerUsuario(): Router {
    return this.router.post('/register', (req, res) => createUserController.handle(req, res));
  }

  login(): Router {
    return this.router.post('/login', (req, res) => loginController.handle(req, res));
  }

  getAll(): Router {
    return this.router.get('/all', Middleware.validationToken, (req, res) =>
      userListController.handle(req, res)
    );
  }

  findById(): Router {
    return this.router.get('/find/:id', Middleware.validationToken, (req, res) =>
      findByidController.handle(req, res)
    );
  }

  recoverPassword(): Router {
    return this.router.get('/recover/:email', (req, res) =>
      recoverPasswordController.handle(req, res)
    );
  }

  resetPassword(): Router {
    return this.router.post('/reset', (req, res) => resetPasswordController.handle(req, res));
  }
}
