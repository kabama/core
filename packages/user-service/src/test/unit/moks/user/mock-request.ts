import { Params } from 'express-serve-static-core';
import { Request } from 'express';
import * as Models from '../../../../core/entities/interfaces';

export function makeMockRequest({
  params,
  query,
  user,
}: {
  params?: Params;
  query?: Params;
  user?: Models.UserDto | undefined;
}): Request {
  const req = {
    params: params || {},
    query: query || {},
    user,
  } as unknown;
  return req as Request;
}
