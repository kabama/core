import * as HttpStatus from 'http-status';

import { NextFunction, Request, Response } from 'express';

import jwt from 'jsonwebtoken';
import { HttpError } from '../../core/errors/index';
import { LogService } from '../../config/logging';
import { config } from '../../config/environments/config';

export const validationToken = (req: Request, res: Response, next: NextFunction) => {
  LogService.info('Auth', 'Token Validated, usuario autorizado');

  const token: any = req.headers.authorization;

  if (token && token.indexOf('Bearer ') !== -1) {
    jwt.verify(
      token.split('Bearer ')[1],
      config.server.token.secret,
      (error: any, decoded: any) => {
        if (error) {
          next(new HttpError(HttpStatus.UNAUTHORIZED, error));
        } else {
          res.locals.jwt = decoded;
          return next();
        }
      }
    );
  } else {
    next(new HttpError(HttpStatus.BAD_REQUEST, 'No token provided'));
  }
};
