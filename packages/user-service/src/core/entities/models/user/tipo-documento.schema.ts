import mongoose, { Schema } from 'mongoose';
import * as Document from '../../interfaces';

export const DocumentTypeSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  desciption: {
    type: String,
    required: true
  }
});

export default mongoose.model<Document.IDocumentTypeDoc>('DocumentType', DocumentTypeSchema);
