import mongoose, { Schema } from 'mongoose';
import * as Models from '../../interfaces';
import * as RecoverMethods from '../../../../functions/signJTW';

const GenderSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  }
});
const DocumentTypeSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  }
});

const AddressesSchema = new Schema({
  city: { type: String },
  country: { type: String },
  direction: { type: String }
});
const UsuarioSchema: Schema = new Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    secondFirstName: { type: String, required: true },
    secondLastName: { type: String, required: true },
    gender: GenderSchema,
    birthday: { type: Number, required: true },
    phone: { type: Number, required: true, unique: true },
    email: {
      type: String,
      unique: true,
      required: true,
      trim: true
    },
    username: {
      type: String,
      unique: true,
      required: true,
      trim: true
    },
    password: { type: String, required: true, max: 100 },

    resetPasswordToken: {
      type: String,
      required: false
    },
    resetPasswordExpires: {
      type: Date,
      required: false
    },
    documentType: DocumentTypeSchema,
    address: [AddressesSchema]
  },
  {
    timestamps: true
  }
);

UsuarioSchema.methods.generatePasswordReset = () => {
  UsuarioSchema.obj.resetPasswordToken = RecoverMethods.generatePasswordReset;
  UsuarioSchema.obj.resetPasswordExpires = RecoverMethods.resetPasswordExpires;
};
const User = mongoose.model<Models.IUser>('Usuario', UsuarioSchema);

export { User };
