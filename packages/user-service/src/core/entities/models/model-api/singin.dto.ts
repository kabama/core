import * as Models from '../../interfaces';

export interface SingInDto {
  token: string;
  message: string;
  user: Models.UserDto;
}
