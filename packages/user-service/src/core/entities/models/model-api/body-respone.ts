export class ResponseBody<T> {
  body: T;

  status?: any;

  constructor(body: T, status?: number) {
    this.body = body;
    this.status = status || 500;
  }
}
