export interface AddressDto {
  city?: string;
  country?: string;
  direction?: string;
}
