import { Document, ObjectId } from 'mongoose';

export interface IDocumentTypeDoc extends Document {
  id: ObjectId;
  name: string;
  description: string;
}
