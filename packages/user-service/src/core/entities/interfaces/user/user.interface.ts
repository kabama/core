import { Document } from 'mongoose';
import { AddressDto } from './address.dto';
import { GenderDto } from './gender.dto';
import { IDocumentTypeDoc } from './tipo-documento.document';

export interface IUser extends Document {
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  secondFirstName: string;
  secondLastName: string;
  birthday: number;
  email: string;
  phone: number;
  resetPasswordToken?: string;
  resetPasswordExpires?: number;
  gender: GenderDto;
  documentType: IDocumentTypeDoc;
  address: AddressDto[];
}
