import * as Dto from '..';

import { GenderDto } from './gender.dto';

export interface UserDto {
  _id?: string;
  username: string;
  email: string;
  password: string | null;
  firstName: string;
  lastName: string;
  secondFirstName: string;
  secondLastName: string;
  gender: GenderDto;
  birthday: number;
  phone: number;
  documentType: Dto.IDocumentTypeDocDto;
  address: Dto.AddressDto[];
}
