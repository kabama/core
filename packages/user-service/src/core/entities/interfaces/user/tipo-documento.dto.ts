export interface IDocumentTypeDocDto {
  id?: string;
  name: string;
  description: string;
}
