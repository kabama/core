export interface GenderDto {
  _id?: string;
  name: string;
  description: string;
}
