export interface IResetPasswordDto {
  resetPasswordToken: string;
  password: string;
}
