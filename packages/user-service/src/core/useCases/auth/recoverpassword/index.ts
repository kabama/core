import { EmailProviderImpl } from '../../../providers/implementations/email-privider.impl';
import { RecoverPasswordController } from './recover-password.controller';
import { RecoverPasswordUseCase } from './recover-pass.case';
import { UserImplRepository } from '../../../../datasources/respository-impl/usuario-respository.impl';

const recoverPasswordUseCase = new RecoverPasswordUseCase(
  new UserImplRepository(),
  new EmailProviderImpl()
);

const recoverPasswordController = new RecoverPasswordController(recoverPasswordUseCase);

export { recoverPasswordUseCase, recoverPasswordController };
