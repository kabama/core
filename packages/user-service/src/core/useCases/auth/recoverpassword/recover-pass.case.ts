import * as Dto from '../../../entities/interfaces';
import * as RecoverMethods from '../../../../functions/signJTW';

import { IEmailProvider } from '../../../providers/imail.provider';
import { UserRepository } from '../../../repositories/user/user.repository';
import { config } from '../../../../config/environments/config';

export class RecoverPasswordUseCase {
  constructor(
    private readonly userRepository: UserRepository,
    private iEmailProvider: IEmailProvider
  ) {}

  async execute(recoverPasswordDto: Dto.IRecoverPasswordDto) {
    const { email } = recoverPasswordDto;
    const user = await this.userRepository.findByEmail(email);

    if (!user) {
      throw new Error(
        `The email address ${email} is not associated with any account. Double-check your email address and try again.'`
      );
    }

    user.resetPasswordToken = RecoverMethods.generatePasswordReset();
    user.resetPasswordExpires = RecoverMethods.resetPasswordExpires();

    user.save().then((userModified) => {
      const link = `http://${config.server.hostname}:${config.server.port}/api/auth/reset/${userModified.resetPasswordToken}`;
      this.iEmailProvider.sendEmail({
        to: {
          name: `${userModified.firstName}${userModified.lastName}`,
          email: userModified.email
        },
        from: {
          name: 'Equipo de linea base',
          email: 'kbmplaying@gmail.com'
        },
        subject: 'Bienvenido a la plataforma!!',
        body: `<h1>Ingrese a la siguiente dirección para recuperar la contraseña</h1><p>${link}</p>`
      });
    });
  }
}
