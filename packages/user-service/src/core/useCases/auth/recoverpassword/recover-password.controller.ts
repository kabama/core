import { Request, Response } from 'express';
import httpStatus from 'http-status';
import * as Dto from '../../../entities/interfaces';

import { RecoverPasswordUseCase } from './recover-pass.case';

export class RecoverPasswordController {
  constructor(private readonly recoverPasswordUseCase: RecoverPasswordUseCase) {}

  async handle(req: Request, res: Response) {
    const { email } = req.params;

    const userRecover: Dto.IRecoverPasswordDto = { email };
    try {
      await this.recoverPasswordUseCase.execute(userRecover);
      return res.status(httpStatus.OK).json({ message: 'sended email recover' });
    } catch (error) {
      return res.status(httpStatus.UNAUTHORIZED).json({ message: error.message });
    }
  }
}
