import * as Dto from '../../../entities/interfaces';
import * as EncryptCore from '../../../../functions/signJTW';

import { IEmailProvider } from '../../../providers/imail.provider';
import { UserRepository } from '../../../repositories/user/user.repository';

export class ResetPasswordUseCase {
  constructor(
    private readonly userRepository: UserRepository,
    private iEmailProvider: IEmailProvider
  ) {}

  async execute(recoverPasswordDto: Dto.IResetPasswordDto) {
    const { resetPasswordToken, password } = recoverPasswordDto;
    const user = await this.userRepository.findByResetTokenAndExpire(resetPasswordToken);

    if (!user) {
      throw new Error('Token has expired');
    }

    EncryptCore.hashPassword(password).then((hash) => {
      if (!hash) {
        throw new Error('No password generate');
      }
      user.password = hash;
      user.resetPasswordExpires = undefined;
      user.resetPasswordToken = undefined;

      user.save().then((userModified) => {
        this.iEmailProvider.sendEmail({
          to: {
            name: `${userModified.firstName}${userModified.lastName}`,
            email: userModified.email,
          },
          from: {
            name: 'Equipo de linea base',
            email: 'kbmplaying@gmail.com',
          },
          subject: 'Cambio de contraseña!!',
          body:
            '<h1>Modificación de contraseña</h1><p>Ya puede ingresar a realizar el login con su nueva contraseña</p>',
        });
      });
    });
  }
}
