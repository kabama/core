import { EmailProviderImpl } from '../../../providers/implementations/email-privider.impl';
import { ResetPasswordController } from './reset-password.controller';
import { ResetPasswordUseCase } from './reset-password.case';
import { UserImplRepository } from '../../../../datasources/respository-impl/usuario-respository.impl';

const resetPasswordUseCase = new ResetPasswordUseCase(
  new UserImplRepository(),
  new EmailProviderImpl()
);

const resetPasswordController = new ResetPasswordController(resetPasswordUseCase);
export { resetPasswordUseCase, resetPasswordController };
