import { Request, Response } from 'express';

import httpStatus from 'http-status';
import { ResetPasswordUseCase } from './reset-password.case';

export class ResetPasswordController {
  constructor(private readonly resetPasswordUseCase: ResetPasswordUseCase) {}

  async handle(req: Request, res: Response) {
    const { resetPasswordToken, password } = req.body;
    try {
      await this.resetPasswordUseCase.execute({ resetPasswordToken, password });
      return res.status(httpStatus.OK).json({ message: 'Password has changed' });
    } catch (error) {
      return res.status(httpStatus.CONFLICT).json({ message: error.message });
    }
  }
}
