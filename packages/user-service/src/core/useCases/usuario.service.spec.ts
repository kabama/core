// import * as DbHandler from "../../test/unit/db-handler";

// import { HttpError } from "../errors/index";
// import { UserDto } from "../entities/interfaces";
// import { UserImplRepository } from "../../datasources/respository-impl/usuario-respository.impl";
// import { UsuarioService } from "./usuario.service";
// import httpStatus from "http-status";

// const usuarioService = new UsuarioService(new UserImplRepository());

// /**Conecta a una nueva bd para test */
// beforeAll(async () => {
//   await DbHandler.connect();
// });

// /**Limpia todas la bd test*/
// afterEach(async () => {
//   await DbHandler.clearDatabase();
// });
// /**Remueve y cierra la bd */
// afterAll(async () => {
//   await DbHandler.closeDatabase();
// });

// describe("Post Create a new User", () => {
//   it("Creando un usuario de forma correcta", async () => {
//     expect(async () => {
//       await usuarioService.register(usuarioCompleto);
//     }).not.toThrow();
//   });

//   it("Creando usuario con username es requerido", async () => {
//     await expect(usuarioService.register(usuarioSinUserName)).resolves.toThrow(
//       new HttpError(
//         httpStatus.INTERNAL_SERVER_ERROR,
//         "Usuario validation failed: username: Path `username` is required."
//       )
//     );
//   });
// });

// describe("GET pruebas para la lectura de los usuarios", () => {
//   it("cuando encuentra usuarios registrados", async () => {
//     expect(async () => {
//       await usuarioService.getAll();
//     }).not.toThrow(new HttpError(httpStatus.NO_CONTENT, "No existen datos"));
//   });

//   it("cuando No encuentra usuarios registrados", async () => {
//     await expect(usuarioService.getAll()).resolves.not.toThrow(
//       new HttpError(httpStatus.NO_CONTENT, "No existen datos")
//     );
//   });
// });

// describe("GET pruebas para la lectura de los usuarios por username", () => {
//   it("cuando encuentra usuarios registrados con username", async () => {
//     expect(async () => {
//       await usuarioService.findByUsername("usuariotest@mail.com");
//     }).not.toThrow();
//   });

//   it("cuando No encuentra usuarios registrado por username", async () => {
//     await expect(usuarioService.findByUsername("noencontrado@mail.com")).resolves.toThrow(
//       new HttpError(httpStatus.NO_CONTENT, "No existen datos")
//     );
//   });
// });

// const usuarioCompleto: UserDto = {
//   password: "123",
//   fullName: "Test usuario 1",
//   phone: 3114154587,
//   username: "usuariotest@mail.com",
// };

// const usuarioSinUserName: any = {
//   password: "123",
//   fullName: "Test usuario 1",
//   phone: 3114154587,
// };
