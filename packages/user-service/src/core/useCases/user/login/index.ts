import { LoginController } from './login-user.controller';
import { LoginUserCase } from './login-user-case';
import { UserImplRepository } from '../../../../datasources/respository-impl/usuario-respository.impl';

const loginUserCase = new LoginUserCase(new UserImplRepository());
const loginController = new LoginController(loginUserCase);

export { loginUserCase, loginController };
