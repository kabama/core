import { Request, Response } from 'express';
import * as ApiModels from '../../../entities/models/model-api';

import { LoginUserCase } from './login-user-case';

export class LoginController {
  constructor(private loginUserCase: LoginUserCase) {}

  async handle(req: Request, res: Response) {
    const user: ApiModels.LoginDto = req.body;
    try {
      const userLogin = await this.loginUserCase.execute(user);

      return res.status(200).json({
        ...userLogin,
      });
    } catch (error) {
      res.status(401).json({
        message: error.message,
        status: 401,
      });
    }
  }
}
