import httpStatus from 'http-status';
import * as ApiModels from '../../../entities/models/model-api';
import * as Dto from '../../../entities/interfaces';
import * as EncryptCore from '../../../../functions/signJTW';

import { UserRepository } from '../../../repositories/user/user.repository';

export class LoginUserCase {
  constructor(private userRepository: UserRepository) {}

  async execute(user: ApiModels.LoginDto) {
    const { email, password } = user;
    const userAlreadyExits = await this.userRepository.findByEmail(email);
    if (!userAlreadyExits) {
      throw new Error('User not exists');
    }

    if (!password) {
      throw new Error('Usuario o password invalidos');
    }
    return EncryptCore.compare(password, userAlreadyExits.password).then((comparePassword) => {
      if (!comparePassword) {
        throw new Error('Usuario o password invalidos');
      } else {
        if (!email) {
          throw new Error('Usuario o password invalidos');
        }

        return EncryptCore.signJWT(email, userAlreadyExits.id)
          .then((token) => {
            if (token) {
              return new ApiModels.ResponseBody<ApiModels.SingInDto>(
                {
                  token,
                  user: {
                    ...this.mapearUserDto(userAlreadyExits)
                  },
                  message: 'Auth success'
                },
                httpStatus.OK
              );
            }
          })
          .catch((errorSigin) => {
            throw new Error('Usuario o password invalidos');
          });
      }
    });
  }

  private mapearUserDto(usuario: Dto.IUser): Dto.UserDto {
    const {
      _id,
      username,
      firstName,
      lastName,
      phone,
      password,
      email,
      secondFirstName,
      secondLastName,
      gender,
      birthday,
      documentType: { name, description },
      address
    } = usuario;
    return {
      _id,
      username,
      firstName,
      lastName,
      phone,
      password,
      email,
      secondFirstName,
      secondLastName,
      gender,
      birthday,
      documentType: { name, description },
      address
    };
  }
}
