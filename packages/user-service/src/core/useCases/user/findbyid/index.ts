import { FindByIdUseCase } from './findbyid.case';
import { FindByidController } from './finbyid.controller';
import { UserImplRepository } from '../../../../datasources/respository-impl/usuario-respository.impl';

const findByIdUseCase = new FindByIdUseCase(new UserImplRepository());

const findByidController = new FindByidController(findByIdUseCase);

export { findByIdUseCase, findByidController };
