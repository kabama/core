import * as Dto from '../../../entities/interfaces';

import { UserRepository } from '../../../repositories/user/user.repository';

export class FindByIdUseCase {
  constructor(private readonly userRepository: UserRepository) {}

  async execute(id: string): Promise<Dto.UserDto> {
    const user = await this.userRepository.findById(id);

    if (!user) {
      throw new Error(`User with ${id} no exists`);
    }

    return this.mapearUserDto(user);
  }

  private mapearUserDto(usuario: Dto.IUser): Dto.UserDto {
    const {
      _id,
      username,
      firstName,
      lastName,
      phone,
      password = null,
      email,
      secondFirstName,
      secondLastName,
      gender,
      birthday,
      documentType: { name, description },
      address
    } = usuario;
    return {
      _id,
      username,
      firstName,
      lastName,
      secondFirstName,
      secondLastName,
      gender,
      birthday,
      phone,
      email,
      password,
      documentType: { name, description },
      address
    };
  }
}
