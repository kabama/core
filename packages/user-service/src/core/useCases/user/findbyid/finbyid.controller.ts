import { Request, Response } from 'express';

import { FindByIdUseCase } from './findbyid.case';

export class FindByidController {
  constructor(private readonly findByIdUseCase: FindByIdUseCase) {}

  async handle(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const user = await this.findByIdUseCase.execute(id);

      return res.status(200).json({ user });
    } catch (error) {
      return res.status(204).json({ message: error.message });
    }
  }
}
