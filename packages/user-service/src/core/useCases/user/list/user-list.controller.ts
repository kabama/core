import { Request, Response } from 'express';

import { UserListCase } from './user-list.case';

export class UserListController {
  constructor(private readonly userListCase: UserListCase) {}

  async handle(req: Request, res: Response) {
    try {
      const users = await this.userListCase.execute();

      return res.status(200).json({ listaUsuarios: users });
    } catch (error) {
      return res.status(204).json({ message: error.message });
    }
  }
}
