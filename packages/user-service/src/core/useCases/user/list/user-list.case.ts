import * as Dto from '../../../entities/interfaces';

import { UserRepository } from '../../../repositories/user/user.repository';

export class UserListCase {
  constructor(private readonly userRepository: UserRepository) {}

  async execute() {
    const users: Dto.IUser[] = await this.userRepository.getAll();
    if (!users) {
      throw new Error('Not exists registers');
    }

    const arrayUser: Dto.UserDto[] = users.map((user: Dto.IUser) => {
      delete user.password;
      return this.mapearUserDto(user);
    });
    return arrayUser;
  }

  private mapearUserDto(usuario: Dto.IUser): Dto.UserDto {
    const {
      _id,
      username,
      firstName,
      lastName,
      secondFirstName,
      secondLastName,
      gender,
      birthday,
      phone,
      password = null,
      email,
      documentType: { name, description },
      address
    } = usuario;
    return {
      _id,
      username,
      firstName,
      lastName,
      phone,
      password,
      email,
      secondFirstName,
      secondLastName,
      gender,
      birthday,
      documentType: { name, description },
      address
    };
  }
}
