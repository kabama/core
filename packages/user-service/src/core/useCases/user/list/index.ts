import { UserImplRepository } from '../../../../datasources/respository-impl/usuario-respository.impl';
import { UserListCase } from './user-list.case';
import { UserListController } from './user-list.controller';

const userListCase = new UserListCase(new UserImplRepository());
const userListController = new UserListController(userListCase);

export { userListCase, userListController };
