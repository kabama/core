import * as DbHandler from '../../../../test/unit/db-handler';

import { UserDto } from '../../../entities/interfaces';
import { createUserCase } from '.';

/** Conecta a una nueva bd para test */
beforeAll(async () => {
  await DbHandler.connect();
});

/** Limpia todas la bd test */
afterEach(async () => {
  await DbHandler.clearDatabase();
});
/** Remueve y cierra la bd */
afterAll(async () => {
  await DbHandler.closeDatabase();
});

describe('Post Create a new User', () => {
  it('Creando usuario con username es requerido', async () => {
    await expect(createUserCase.execute(usuarioSinUserName)).rejects.toThrow(
      new Error('Username invalid')
    );
  });

  it('Creando un usuario de forma correcta', async () => {
    expect(async () => {
      await createUserCase.execute(usuarioCompleto);
    }).not.toThrow();
  });

  it('Creando usuario con username que existe', async () => {
    await createUserCase.execute(usuarioCompleto);
    await expect(createUserCase.execute(usuarioCompleto)).rejects.toThrow('User already exists');
  });

  it('Creando usuario con sin contraseña', async () => {
    await expect(createUserCase.execute(usuarioSinPassword)).rejects.toThrow('Contraseña invalida');
  });
});

const usuarioCompleto: UserDto = {
  password: '123',
  fullName: 'Test usuario 1',
  phone: 3114154587,
  username: 'usuariotest@mail.com',
};

const usuarioExists: UserDto = {
  password: '123456',
  fullName: 'Test usuario 1',
  phone: 3114154587,
  username: 'usuariotest@mail.com',
};

const usuarioSinUserName: any = {
  password: '123',
  fullName: 'Test usuario 1',
  phone: 3114154587,
};

const usuarioSinPassword: any = {
  fullName: 'Test usuario 1',
  phone: 3114154587,
  username: 'nuevosinpassword@gmail.com',
};

const usuarioPasswordNull: any = {
  password: null,
  fullName: 'Test usuario 1',
  phone: 3114154587,
  username: 'nuevosinpassword@gmail.com',
};
