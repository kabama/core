import { CreateUserCase } from './create-user.case';
import { CreateUserController } from './create-user.controller';
import { EmailProviderImpl } from '../../../providers/implementations/email-privider.impl';
import { UserImplRepository } from '../../../../datasources/respository-impl/usuario-respository.impl';

const userImplRepository = new UserImplRepository();
const emailProviderImpl = new EmailProviderImpl();
const createUserCase = new CreateUserCase(userImplRepository, emailProviderImpl);

const createUserController = new CreateUserController(createUserCase);

export { createUserCase, createUserController };
