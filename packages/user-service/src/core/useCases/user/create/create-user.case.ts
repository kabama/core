import * as Dto from '../../../entities/interfaces';
import * as EncryptCore from '../../../../functions/signJTW';

import { IEmailProvider } from '../../../providers/imail.provider';
import { User } from '../../../entities/models/user/user.schema';
import { UserRepository } from '../../../repositories/user/user.repository';
import mongoose from 'mongoose';

export class CreateUserCase {
  constructor(private userRepository: UserRepository, private emailProvider: IEmailProvider) {}

  async execute(user: Dto.UserDto): Promise<void | Error> {
    const {
      username,
      firstName,
      lastName,
      phone,
      password,
      email,
      documentType: { description, name },
      secondFirstName,
      secondLastName,
      gender,
      birthday,
      address
    } = user;

    if (!username) {
      throw new Error('Username invalid');
    }
    const userAlreadyExists = await this.userRepository.findByEmail(username);
    if (userAlreadyExists) {
      throw new Error('User already exists');
    }

    if (!password) {
      throw new Error('Contraseña invalida');
    }

    EncryptCore.hashPassword(password).then((hash) => {
      if (!hash) {
        throw new Error('No password generate');
      }

      const userData = new User({
        username,
        firstName,
        lastName,
        secondFirstName,
        secondLastName,
        gender,
        birthday,
        phone,
        password: hash,
        email,
        id: new mongoose.Types.ObjectId(),
        documentType: { description, name },
        address
      });

      this.userRepository.save(userData);

      this.emailProvider.sendEmail({
        to: {
          name: `${userData.firstName}${userData.lastName}`,
          email: userData.username
        },
        from: {
          name: 'Equipo de linea base',
          email: 'kbmplaying@gmail.com'
        },
        subject: 'Bienvenido a la plataforma!!',
        body:
          '<h1>Bienvenido</h1><p>Buen día, sea bienvenido a nuestra plataforma ya puedes hacer login.</p>'
      });
    });
  }
}
