import { Request, Response } from 'express';

import httpStatus from 'http-status';
import { CreateUserCase } from './create-user.case';
import { HttpError } from '../../../errors/index';
import { UserDto } from '../../../entities/interfaces';

export class CreateUserController {
  constructor(private usercase: CreateUserCase) {}

  async handle(req: Request, res: Response) {
    try {
      const user: UserDto = req.body;
      this.usercase.execute(user);
      return res.status(201).send();
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        ...new HttpError(httpStatus.INTERNAL_SERVER_ERROR, error.message)
      });
    }
  }
}
