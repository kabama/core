import httpStatus from 'http-status';
import mongoose from 'mongoose';
import * as ApiModels from '../entities/models/model-api';
import * as EncryptCore from '../../functions/signJTW';
import * as Models from '../entities/interfaces';

import { HttpError } from '../errors/index';
import User from '../entities/models/user/user.schema';
import { UserRepository } from '../repositories/user/user.repository';

export class UsuarioService {
  constructor(private readonly repository: UserRepository) {}

  // : Promise<ApiModels.ResponseBody<Models.UserDto> | ApiModels.ErrorResponse>
  async register(usuario: Models.UserDto) {
    const { username, password, phone, firstName, lastName, email } = usuario;
    if (!password) {
      return new HttpError(httpStatus.INTERNAL_SERVER_ERROR, 'Contraseña invalida');
    }
    return EncryptCore.hashPassword(password).then((hash) => {
      if (!hash) {
        return new HttpError(httpStatus.INTERNAL_SERVER_ERROR, 'No password generate');
      }
      const user: Models.IUser = new User({
        username,
        password: hash,
        phone,
        firstName,
        lastName,
        email,
        id: new mongoose.Types.ObjectId(),
      });
      return this.repository
        .save(user)
        .then(
          (user) =>
            new ApiModels.ResponseBody<Models.UserDto>(this.mapearUserDto(user), httpStatus.OK)
        )
        .catch((err) => new HttpError(httpStatus.INTERNAL_SERVER_ERROR, err.message));
    });
  }

  async getAll(): Promise<ApiModels.ResponseBody<Models.UserDto[]> | HttpError> {
    const users: Models.IUser[] = await this.repository.getAll();
    if (!users) {
      return new HttpError(httpStatus.NO_CONTENT, 'No existen datos');
    }
    const arrayUser: Models.UserDto[] = users.map((user: Models.IUser) => {
      delete user.password;
      return {
        ...this.mapearUserDto(user),
      };
    });
    return new ApiModels.ResponseBody(arrayUser, httpStatus.OK);
  }

  async findByUsername(prop: string): Promise<HttpError | ApiModels.ResponseBody<Models.UserDto>> {
    const user = await this.repository.findByUsername(prop);

    if (!user) {
      return new HttpError(httpStatus.NO_CONTENT, 'No existen datos');
    }
    return new ApiModels.ResponseBody<Models.UserDto>(this.mapearUserDto(user), httpStatus.OK);
  }

  async login(
    username: string,
    password: string
  ): Promise<ApiModels.ResponseBody<ApiModels.SingInDto> | HttpError> {
    return this.repository.findByUsername(username).then((user) => {
      if (!password) {
        return new HttpError(httpStatus.INTERNAL_SERVER_ERROR, 'Usuario o password invalidos');
      }
      return EncryptCore.compare(password, user.password).then((comparePassword) => {
        if (!comparePassword) {
          return new HttpError(httpStatus.UNAUTHORIZED, 'Usuario o password invalidos');
        }
        return EncryptCore.signJWT(password, user.id)
          .then((token) => {
            if (token) {
              return new ApiModels.ResponseBody<ApiModels.SingInDto>(
                {
                  token,
                  user: {
                    ...this.mapearUserDto(user),
                  },
                  message: 'Auth success',
                },
                httpStatus.OK
              );
            }
          })
          .catch(
            (errorSigin) => new HttpError(httpStatus.UNAUTHORIZED, 'Usuario o password invalidos')
          );
      });
    });
  }

  private mapearUserDto(usuario: Models.IUser): Models.UserDto {
    const { _id, firstName, lastName, email, phone, username } = usuario;
    return {
      _id,
      firstName,
      lastName,
      email,
      phone,
      username,
      password: null,
    };
  }
}
