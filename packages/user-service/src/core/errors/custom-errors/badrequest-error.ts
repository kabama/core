import { BAD_REQUEST_STATUS_CODE, BAD_REQUEST_STATUS_MESSAGE } from 'config/constants/constants';

export class BadRequestException extends Error {
  private statusCode: number;

  constructor(message?: string) {
    message = !message ? BAD_REQUEST_STATUS_MESSAGE : message;
    super(message);
    this.statusCode = BAD_REQUEST_STATUS_CODE;
  }

  get status() {
    return this.statusCode;
  }
}
