import { NOT_FOUND_STATUS_CODE } from 'config/constants/constants';

export class NotFoundException extends Error {
  private statusCode: number;

  constructor(message: string) {
    super(message);
    this.statusCode = NOT_FOUND_STATUS_CODE;
  }

  get status() {
    return this.statusCode;
  }
}
