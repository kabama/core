import { INTERNAL_SERVER_MESSAGE, INTERNAL_SERVER_STATUS_CODE } from 'config/constants/constants';

export class ServerException extends Error {
  private statusCode: number;

  constructor(message?: string) {
    message = !message ? INTERNAL_SERVER_MESSAGE : message;
    super(message);
    this.statusCode = INTERNAL_SERVER_STATUS_CODE;
  }

  get status() {
    return this.statusCode;
  }
}
