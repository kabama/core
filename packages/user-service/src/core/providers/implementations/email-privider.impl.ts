import Mail from 'nodemailer/lib/mailer';
import nodemailer from 'nodemailer';
import { IEmailProvider, IMessage } from '../imail.provider';

export class EmailProviderImpl implements IEmailProvider {
  private transporter: Mail;

  constructor() {
    this.transporter = nodemailer.createTransport({
      host: 'smtp.mailtrap.io',
      port: 2525,
      auth: { user: '997001ef2a25af', pass: '82fc4a78075d2a' },
    });
  }

  sendEmail(message: IMessage): Promise<void> {
    return this.transporter.sendMail({
      to: {
        name: message.to.name,
        address: message.to.email,
      },
      from: {
        name: message.from.name,
        address: message.from.email,
      },
      subject: message.subject,
      html: message.body,
    });
  }
}
