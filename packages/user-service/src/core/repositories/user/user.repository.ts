import * as Models from '../../entities/interfaces';

export interface UserRepository {
  save(usuario: Models.IUser): Promise<Models.IUser>;
  getAll(): Promise<Models.IUser[]>;
  findByUsername(username: string): Promise<Models.IUser>;
  findById(id: string): Promise<Models.IUser>;
  findByEmail(email: string): Promise<Models.IUser>;
  findByResetTokenAndExpire(resetPasswordToken: string): Promise<Models.IUser>;
}
