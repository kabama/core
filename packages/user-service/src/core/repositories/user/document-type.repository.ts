import * as Models from '../../entities/interfaces';
import * as Repository from '..';

export interface DocumentTypeRepository
  extends Repository.CrudRepository<Models.IDocumentTypeDoc> {}
