import * as Models from '../../entities/interfaces';
import * as Repository from '..';

export interface GenreRepository extends Repository.CrudRepository<Models.G> {}
