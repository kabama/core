import { Document } from 'mongoose';

export interface CrudRepository<R extends Document> {
  findAll(): Promise<R[]>;
  save(t: R): Promise<void>;
  findOne(property: string | number): Promise<R>;
  deleteById(id: string): Promise<void>;
}
