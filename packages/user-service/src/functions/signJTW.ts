import bcryptjs from 'bcryptjs';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import { config } from '../config/environments/config';
import { LogService } from '../config/logging';

const NAMESPACE = 'Auth';
const HASHROUND: number = 10;
const EXPIREDATE = 3600000;
const RANDOMBYTENUMBER = 20;
export const signJWT = (username: string, id: string) => {
  const timeSinceEpoch = new Date().getTime();
  const expirationTime = timeSinceEpoch + Number(config.server.token.expireTime) * 100000;
  const expirationTimeInSeconds = Math.floor(expirationTime / 1000);

  LogService.info(NAMESPACE, `Attempting to sign token for ${id}`);

  return new Promise<string>((resolve, reject) => {
    jwt.sign(
      {
        username
      },
      config.server.token.secret,
      {
        issuer: config.server.token.issuer,
        algorithm: 'HS256',
        expiresIn: expirationTimeInSeconds
      },
      (error, token) => {
        if (error) {
          return reject(error);
        }
        if (token) {
          return resolve(token);
        }
      }
    );
  });
};

export const compare = (password: string, has: string): Promise<boolean | Error> =>
  bcryptjs.compare(password, has);

export const hashPassword = (password: string): Promise<string> =>
  bcryptjs.hash(password, HASHROUND);

export const generatePasswordReset = () => {
  return crypto.randomBytes(RANDOMBYTENUMBER).toString('hex');
};

export const resetPasswordExpires = () => Date.now() + EXPIREDATE;
